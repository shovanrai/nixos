# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./apps/vim
      ./apps/nvim
    ];
  nixpkgs.config.allowBroken = true; 
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
             "discord" 
             "viber"
             "slack"
           ];

  # STORAGE OPTIMIZATION
  #nix.autoOptimiseStore = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "nixos-desktop"; # Define your hostname.
  networking.networkmanager.enable = true; # Added later
  #networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Set your time zone.
  time.timeZone = "Asia/Kathmandu";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp1s0.useDHCP = true;
  networking.interfaces.wlp2s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";
  
  # Enable 3001 for cardano node
  networking.firewall.allowedTCPPorts = [ 3001 1337  5001] ;

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  # };

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Configure keymap in X11
  services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";
  
  # Window Manageer
  services.xserver.displayManager.lightdm.enable = true;
  services.xserver.windowManager = {
    xmonad.enable = true;
    xmonad.enableContribAndExtras = true;
    xmonad.extraPackages = hpkgs: [
        hpkgs.xmonad
        hpkgs.xmonad-contrib
        hpkgs.xmonad-extras
        ];
  };
  

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  hardware.bluetooth.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;
  # Enable blueman 
  services.blueman.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.shovanrai = {
    isNormalUser = true;
    initialPassword = "password";
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
   environment.systemPackages = with pkgs; [
    # BASE
    wget
    firefox
    nano
    git
    xmobar
    alacritty
    htop
    screenfetch
    rxvt-unicode
    
    #### After base install
    nix-prefetch-github
    trayer
    picom
    ranger
    dmenu
    lf
    nnn
    procs
    pcmanfm
    bat
    rofi
    pa_applet
    networkmanagerapplet
   
    ### SYSTEM 
    xorg.xmodmap
    xcape
    docker
    docker-compose
    postgresql_10
    glances
    redshift
    jq # json processor
    killall
    blueman

    ### PERSONAL
    freetube
    mpd
    vlc
    mpv
    ncmpcpp
    keepassxc
    nitrogen
    neovim
    brave
    screenfetch
    exa
    sxiv
    mupdf
    tdesktop
    notepadqq
    fzf
    qbittorrent
    thunderbird
    discord
    element-desktop
    viber
    slack
    signal-desktop
    remmina
    powerline
    starship
    tokei # count your code
    fd    # rust replacement for find
    
    ### Developement
    vscodium
    gcc
    rnix-lsp
    haskell-language-server
    pyright
    python39Packages.black
    python39Packages.flake8
    ripgrep
    haskellPackages.hlint
    # haskellPackages.ghc-mod
    #haskellPackages.hdevtools
    #espeak 
    #iPhone
    # ifuse
    #usbmuxd
    #libplist
    #libimobiledevice

    ### ADVANCED
    nixFlakes
    ];

  # Fonts for all applications  
  fonts.fonts = with pkgs; [
    noto-fonts
    noto-fonts-emoji
    liberation_ttf
    fira-code
    fira-code-symbols
    mplus-outline-fonts
    dina-font
    proggyfonts
    nerdfonts
    powerline-fonts
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  
  # PLUTUS PIONEER iohk cache
  nix = {
    settings.substituters          = [ "https://hydra.iohk.io" "https://iohk.cachix.org" ];
    settings.trusted-public-keys = [ "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ=" "iohk.cachix.org-1:DpRUyj7h7V830dp/i6Nti+NEO2/nhblbov/8MW7Rqoo=" ];
    package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
};
  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?

}

