set encoding=utf-8
"call plug#begin()
"Plug 'preservim/NERDTree'
"Plug 'davidhalter/jedi-vim'
"Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'} " highlight
"Plug 'neoclide/coc.nvim', {'branch': 'release'}
"Plug 'neovimhaskell/haskell-vim'
"Plug 'dense-analysis/ale'
"Plug 'Vimjas/vim-python-pep8-indent'
"Plug 'joshdick/onedark.vim'
"Plug 'vim-syntastic/syntastic'
"Plug 'nvie/vim-flake8'
"Plug 'kien/ctrlp.vim'
"Plug 'wakatime/vim-wakatime'
"Plug 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
"Plug 'xolox/vim-misc'
"Plug 'xolox/vim-session'
"Plug 'junegunn/fzf', { 'do': { -> fzf#install() } } 
"Plug 'junegunn/fzf.vim'
"Plug 'tpope/vim-commentary'
"Plug 'autozimu/LanguageClient-neovim', {
"    \ 'branch': 'next',
"    \ 'do': './install.sh'
"    \ }
"
"call plug#end()

" let g:jedi#use_tabs_not_buffers = 1
" let g:jedi#use_splits_not_buffers = "left"
" Syntactic Settings
let g:syntastic_python_checkers = ["flake8"]
let g:syntastic_flake8_max_line_length="160"
let g:syntastic_auto_loc_list=1

inoremap jk <ESC>
set hlsearch
set noswapfile
set ignorecase
"set spell spelllang=en_us
"pretty code
let python_highlight_all=1
syntax on
set expandtab
set tabstop=4
set hidden
"colorscheme solarized
"PEP-8 Indentation
"au BufNewFile,BufRead *.py
"    \ set tabstop=4 |
"    \ set softtabstop=4 |
"    \ set shiftwidth=4 |
"    \ set textwidth=200 |
"    \ set expandtab |
"    " \ set autoindent |
"    \ set fileformat=unix

" Enable folding
set foldmethod=indent
set foldlevel=99

set incsearch
set scrolloff=8
set signcolumn=no

nmap <space> za
nmap <F2> :NERDTreeToggle<CR>
nmap <F3> :NERDTreeFind<CR>
:nnoremap <F5> :buffers<CR>:buffer<Space>
"set number
:set number relativenumber

" silver-search-git'
map <C-s> :Ag

"clipboard
vnoremap <C-y> "+y
map <C-k> "+p
:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END
set mouse=a
"mouse support for alacritty
set ttymouse=sgr

"Bracket detect
let loaded_matchparen = 1

:let g:session_autoload = 'no'

" Tab completion 
function! Tab_Or_Complete()
  if col('.')>1 && strpart( getline('.'), col('.')-2, 3 ) =~ '^\w'
    return "\<C-N>"
  else
    return "\<Tab>"
  endif
endfunction
:inoremap <Tab> <C-R>=Tab_Or_Complete()<CR>
":set dictionary="/usr/dict/words"
