{ pkgs, ... }:
{
 environment.variables = { EDITOR = "vim";};
 environment.systemPackages = with pkgs; [ 
  ((vim_configurable.override { python = python3; }).customize {
    name = "vim";
    vimrcConfig.packages.myplugins = with pkgs.vimPlugins; {
      start = [               # Plugins loaded on launch
        airline               # Lean & mean status/tabline for vim that's light as air
        vim-airline-themes    # Collection of themes for airlin
        vim-nix               # Support for writing Nix expressions in vim
        nerdtree               
        fzf-vim
        awesome-vim-colorschemes
        YouCompleteMe
        flake8-vim
        ale
        vim-commentary
        ];
      #opt = [flake8-vim ];
      # autocmd FileType php :packadd phpCompletion
     # autocmd.FileType python packadd vim-flake8;
    };
    vimrcConfig.customRC = ''
      "autocmd FileType python packadd! flake8-vim
      "autocmd FileType python packadd black
      autocmd BufWritePost *.py call flake8-vim
      "autocmd BufWritePre *.py execute ':Black'
      autocmd FileType sh,python,text setlocal commentstring=#%s
      autocmd FileType xml,html setlocal commentstring=<!--%s-->
      
      let mapleader=","             " leader is comma
      nnoremap <leader><space> :nohlsearch<CR> " turn off search highlight
      nnoremap <leader>s :mksession<CR> " save session
      noremap <Leader>W :w !sudo tee % > /dev/null
      "autocmd FileType python nnoremap <leader>= :0,$!yapf<CR>
      "nnoremap <leader>u :GundoToggle<CR> " toggle gundo
      noremap <leader>/ :Commentary<cr>

      set bs=2
      set smartindent
      set tabstop=4                 " Default tabstop
      set shiftwidth=4              " Default indent spacing
      set expandtab                 " Expand [TABS] to spaces
      set softtabstop=4
      set textwidth=79
      set autoindent
      set fileformat=unix
      syntax enable                 " Enable syntax highlighting
      set t_Co=256                  " use 265 colors in vim
      set showcmd
      set cursorline
      filetype indent on
      set wildmenu                  " Visual autocomplete for command menu
      set lazyredraw                " redraw only when we need to.
      set hlsearch
      set noswapfile
      set ignorecase
      set hidden
      set foldmethod=indent
      set foldlevel=99
      set incsearch
      set scrolloff=8
      set signcolumn=no
      set number relativenumber
      
      set mouse=a
      set ttymouse=sgr
     
      "KEY-MAPPINGS    
      map <C-s> :FZF
      nmap <space> za
      nmap <F2> :NERDTreeToggle<CR>
      nnoremap<F5> :buffers<CR>:buffer<space>
      noremap <C-y> "+y
      inoremap jk <ESC>
      vnoremap <C-i> "+p
      
      :augroup numbertoggle
      :  autocmd!
      :  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
      :  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
      :augroup END

      " Tab completion
      function! Tab_Or_Complete()
      if col('.')>1 && strpart( getline('.'), col('.')-2, 3 ) =~ '^\w'
        return "\<C-N>"
      else
        return "\<Tab>"
      endif
      endfunction
      :inoremap <Tab> <C-R>=Tab_Or_Complete()<CR>

      let g:airline_theme='badwolf'
      let g:airline#extensions#tabline#enabled=1
      let g:airline#extensions#tabline#formatter='unique_tail'
      let g:airline_powerline_fonts=1
        
        '';
})];
}
