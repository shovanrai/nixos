{ pkgs, ... }:
{
 environment.variables = { EDITOR = "vim";};
 environment.systemPackages = with pkgs; [ 
  ((vim_configurable.override { python = python3; }).customize {
    name = "vim";
    vimrcConfig.packages.myplugins = with pkgs.vimPlugins; {
      start = [               # Plugins loaded on launch
        airline               # Lean & mean status/tabline for vim that's light as air
        vim-airline-themes    # Collection of themes for airlin
        vim-nix               # Support for writing Nix expressions in vim
        nerdtree               
        fzf-vim
        awesome-vim-colorschemes
        YouCompleteMe
        flake8-vim
        ale
        vim-commentary
        ];
      #opt = [flake8-vim ];
      # autocmd FileType php :packadd phpCompletion
     # autocmd.FileType python packadd vim-flake8;
    };
    vimrcConfig.customRC = builtins.readFile ./.vimrc;
})];
}
