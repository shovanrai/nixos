with import <nixpkgs> {};

let inherit (vimUtils) buildVimPluginFrom2Nix; in {
   "flake-8" = buildVimPluginFrom2Nix {
    name = "flake-8";
    src = fetchgit {
        url ="https://github.com/nvie/vim-flake8" ;
        rev = "5bd58732be452a32e923179673b94e5f1ed5dc9e";
        sha256 = "C6c27Sr1CnlQoQPAq2Di0C1kBBdEGrEabT2EzbS16XQ=" ;
    };
};
 }
