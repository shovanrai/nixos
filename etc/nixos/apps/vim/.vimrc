set bs=2
set smartindent
set tabstop=4                 " Default tabstop
set shiftwidth=4              " Default indent spacing
set expandtab                 " Expand [TABS] to spaces
set softtabstop=4
set textwidth=79
set autoindent
set fileformat=unix
set t_Co=256                  " use 265 colors in vim
set showcmd
set cursorline
set nohlsearch
set wildmenu                  " Visual autocomplete for command menu
set lazyredraw                " redraw only when we need to.
set hlsearch
nnoremap <leader><space> :nohlsearch<CR> " turn off search highlight
set noswapfile
set ignorecase
set smartcase
set hidden
set nowrap
set foldmethod=indent
set foldlevel=99
set incsearch
set scrolloff=8
set colorcolumn=100
set signcolumn=yes
set number relativenumber

set mouse=a
set ttymouse=sgr

syntax enable                 " Enable syntax highlighting
filetype indent on

"KEY-MAPPINGS"
let mapleader=","             " leader is comma
nnoremap <leader>s :mksession<CR> " save session
noremap <Leader>W :w !sudo tee % > /dev/null
"nnoremap <leader>u :GundoToggle<CR> " toggle gundo
noremap <leader>/ :Commentary<cr>
noremap <Leader>jd :YcmCompleter GoTo<CR>
nnoremap <Leader>fi :YcmCompleter FixIt<CR>
nnoremap <Leader>l :vertical resize +5<CR>
nnoremap <Leader>h :vertical resize -5<CR>

map <C-s> :!fzf
nmap <space> za
map <F2> :NERDTreeToggle<CR>
noremap<F5> :buffers<CR>:buffer<space>
noremap <C-y> "+y
noremap <C-i> "+p
inoremap jk <ESC>
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-l> <C-W>l
map <C-h> <C-W>h

"autocmd FileType python nnoremap <leader>= :0,$!yapf<CR>
"autocmd BufWritePost *.py call Flake-8
"autocmd BufWritePre *.py execute ':Black'
autocmd FileType sh,python,text setlocal commentstring=#%s
autocmd FileType xml,html setlocal commentstring=<!--%s-->

:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

" Tab completion
function! Tab_Or_Complete()
if col('.')>1 && strpart( getline('.'), col('.')-2, 3 ) =~ '^\w'
  return "\<C-N>"
else
  return "\<Tab>"
endif
endfunction
:inoremap <Tab> <C-R>=Tab_Or_Complete()<CR>

let g:airline_theme='badwolf'
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#tabline#formatter='unique_tail'
let g:airline_powerline_fonts=1

let g:ale_linters = {'python': ['flake8']}
let g:ale_fixers = {
    \   '*': ['remove_trailing_lines', 'trim_whitespace'],
    \   'python': ['black'],
    \}
let g:ale_fix_on_save =1

let g:fzf_preview_window = 'right:50%'
let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6  }  }
