{ pkgs, ... }:

with pkgs;
let hvim = neovim.override {
  configure = {
    vam.knownPlugins = pkgs.vimPlugins; # // import ./vimPlugins.nix;
    vam.pluginDictionaries = [
      {
        names = [
          "nerdtree"
          "airline"
          "vim-airline-themes"
          "awesome-vim-colorschemes"
          "vim-commentary"
          "nvim-lspconfig"
          "stylish-haskell"
          "nvim-cmp"
          "nvim-autopairs"
          "lspkind-nvim"
          "cmp-nvim-lsp"
          "telescope-nvim"
          "telescope-file-browser-nvim"
          "plenary-nvim"
          "nvim-web-devicons"
          "nvim-treesitter"
        ];
      }
    ];
    customRC = builtins.readFile ./.vimrc;
  };
};
in {
  environment.systemPackages = with pkgs; [hvim ];
}
