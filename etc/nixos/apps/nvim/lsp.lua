require 'lspconfig'.rnix.setup{}
require 'lspconfig'.pyright.setup{}
require 'lspconfig'.hls.setup{
    settings = {
        formattingProvider = 'stylish-haskell',
    },
    on_attach = on_attach,
}
