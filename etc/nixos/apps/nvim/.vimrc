set bs=2
set smartindent
set tabstop=4                 " Default tabstop
set shiftwidth=4              " Default indent spacing
set expandtab                 " Expand [TABS] to spaces
set softtabstop=4
set textwidth=79
set autoindent
set fileformat=unix
set t_Co=256                  " use 265 colors in vim
set showcmd
set cursorline
set nohlsearch
set wildmenu                  " Visual autocomplete for command menu
set lazyredraw                " redraw only when we need to.
set hlsearch
nnoremap <leader><,> :nohlsearch<CR> " turn off search highlight
set noswapfile
set ignorecase
set smartcase
set hidden
set nowrap
set foldmethod=indent
set foldlevel=99
set scrolloff=8
set colorcolumn=100
set signcolumn=yes
set number relativenumber
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()

set mouse=a
" set ttymouse=sgr

syntax on                 " enable syntax highlighting
filetype indent on
filetype plugin indent on

"KEY-MAPPINGS"
let mapleader=" "             " leader is space
nnoremap <leader>s :mksession<CR> " save session
noremap <Leader>W :w !sudo tee % > /dev/null
"nnoremap <leader>u :GundoToggle<CR> " toggle gundo
noremap <leader>/ :Commentary<cr>
nnoremap <Leader>l :vertical resize +5<CR>
nnoremap <Leader>h :vertical resize -5<CR>

" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>
nnoremap <leader>fe <cmd>Telescope file_browser<cr>

" LSP config 
nnoremap <silent> gd <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent> gD <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> gr <cmd>lua vim.lsp.buf.references()<CR>
nnoremap <silent> gi <cmd>lua vim.lsp.buf.implementation()<CR>
nnoremap <silent> K <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> <C-k> <cmd>lua vim.lsp.buf.signature_help()<CR>
nnoremap <silent> <C-n> <cmd>lua vim.lsp.diagnostic.goto_prev()<CR>
nnoremap <silent> <C-p> <cmd>lua vim.lsp.diagnostic.goto_next()<CR>

map <F2> :NERDTreeToggle<CR>
noremap<F5> :buffers<CR>:buffer<space>
noremap <C-y> "+y
noremap <C-i> "+p
inoremap jk <ESC>
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-l> <C-W>l
map <C-h> <C-W>h
nmap <leader>1 :bp<CR>
nmap <leader>2 :bn<CR>
nmap <leader>3 :bd<CR>

let NERDTreeShowHidden=1
let NERDTreeQuitOnOpen=1
let NERDTreeMinimalUI=1
let g:haskell_enable_quantification = 1   " to enable highlighting of `forall`
let g:haskell_enable_recursivedo = 1      " to enable highlighting of `mdo` and `rec`
let g:haskell_enable_arrowsyntax = 1      " to enable highlighting of `proc`
let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
let g:haskell_enable_typeroles = 1        " to enable highlighting of type roles
let g:haskell_enable_static_pointers = 1  " to enable highlighting of `static`
let g:haskell_backpack = 1                " to enable highlighting of backpack keywords
" let g:haskell_classic_highlighting = 1

let g:haskell_indent_if = 3
let g:haskell_indent_case = 2
let g:haskell_indent_let = 4
let g:haskell_indent_where = 6
let g:haskell_indent_before_where = 2
let g:haskell_indent_after_bare_where = 2
let g:haskell_indent_do = 3
let g:haskell_indent_in = 1
let g:haskell_indent_guard = 2
let g:cabal_indent_section = 2

:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

" Tab completion
function! Tab_Or_Complete()
if col('.')>1 && strpart( getline('.'), col('.')-2, 3 ) =~ '^\w'
  return "\<C-N>"
else
  return "\<Tab>"
endif
endfunction
:inoremap <Tab> <C-R>=Tab_Or_Complete()<CR>

let g:airline_theme='badwolf'
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#tabline#formatter='unique_tail'
let g:airline_powerline_fonts=1

luafile /etc/nixos/apps/nvim/lsp.lua
luafile /etc/nixos/apps/nvim/nvim-cmp.lua
luafile /etc/nixos/apps/nvim/nvim-autopairs.lua
luafile /etc/nixos/apps/nvim/telescope.lua
luafile /etc/nixos/apps/nvim/treesitter.lua
