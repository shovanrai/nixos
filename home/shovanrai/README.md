### Description of the file content

```/etc/nisos/configuration.xix```
Contains the configuration for the main system.

```/etc/nisos/hardware-configuration.nix```
Auto-Generated during NixOS Installation, Can be changed for advanced settings.

```/etc/nisos/apps```
For Configuring apps in a Deterministic way. It contains vim and nvim config
