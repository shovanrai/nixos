#!/usr/bin/env bash
#
# Script name: dmconf
# Description: Choose from a list of configuration files to edit.
# Dependencies: dmenu
# GitLab: https://www.gitlab.com/dwt1/dmscripts
# Contributors: Derek Taylor

# Defining the text editor to use.
# DMENUEDITOR="st -e vim"
# DMENUEDITOR="st -e nvim"
DMEDITOR="alacritty -e vim"

# An array of options to choose.
# You can edit this list to add/remove config files.
declare -a options=(
"nixos-configx - /etc/nixos/configuration.nix"
"dmconfig - $HOME/scripts/dmconfig"
"xmonad - $HOME/.xmonad/xmonad.hs"
"dmsearch - $HOME/scripts/dmsearch"
"alacritty - $HOME/.config/alacritty/alacritty.yml"
"xmobar0 - $HOME/.config/xmobar/primary.hs"
"xmobar1 - $HOME/.config/xmobar/secondary.hs"
"bash - $HOME/.bashrc"
"bspwm - $HOME/.config/bspwm/bspwmrc"
"polybar - $HOME/.config/polybar/config"
"picom - $HOME/.config/picom/picom.conf"
"dunst - $HOME/.config/dunst/dunstrc"
#"dwm - $HOME/dwm-distrotube/config.def.h"
#"dwmblocks - $HOME/dwmblocks-distrotube/blocks.def.h"
#"fish - $HOME/.config/fish/config.fish"
"i3 - $HOME/.config/i3/config"
#"neovim - $HOME/.config/nvim/init.vim"
#"picom - $HOME/.config/picom/picom.conf"
"polybar - $HOME/.config/polybar/config"
#"qtile - $HOME/.config/qtile/config.py"
"quickmarks - $HOME/.config/qutebrowser/quickmarks"
#"qutebrowser - $HOME/.config/qutebrowser/autoconfig.yml"
#"spectrwm - $HOME/.spectrwm.conf"
#"stumpwm - $HOME/.config/stumpwm/config"
#"surf - $HOME/surf-distrotube/config.def.h"
"sxhkd - $HOME/.config/sxhkd/sxhkdrc"
#"tabbed - $HOME/tabbed-distrotube/config.def.h"
"termite - $HOME/.config/termite/config"
#"vifm - $HOME/.config/vifm/vifmrc"
"vim - /etc/nixos/apps/vim/vim.nix" 
#"xmobar mon1  - $HOME/.config/xmobar/xmobarrc0"
#"xmobar mon2 - $HOME/.config/xmobar/xmobarrc1"
#"xmobar mon3 - $HOME/.config/xmobar/xmobarrc2"
"xresources - $HOME/.Xresources"
#"zsh - $HOME/.zshrc"
"tmux - $HOME/.tmux.conf"
"quit"
)

# Piping the above array into dmenu.
# We use "printf '%s\n'" to format the array one item to a line.
choice=$(printf '%s\n' "${options[@]}" | dmenu -l 20 -p 'Edit config:')

# What to do when/if we choose 'quit'.
if [[ "$choice" == "quit" ]]; then
    echo "Program terminated." && exit 1

# What to do when/if we choose a file to edit.
elif [ "$choice" ]; then
	cfg=$(printf '%s\n' "${choice}" | awk '{print $NF}')
	$DMEDITOR "$cfg"

# What to do if we just escape without choosing anything.
else
    echo "Program terminated." && exit 1
fi

