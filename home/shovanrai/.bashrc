#!/bin/bash

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# SYSTEM
# PS1='[\u@\h \W]\$ '
shopt -s autocd
export HISTSIZE=-1
export HISTFILESIZE=-1
export HISTCONTROL=ignoreboth
export PATH="$PATH:$HOME/.local/bin"

# ALIAS
alias logout='pkill -KILL -U shovanrai'
alias grep='grep --color=auto'
alias mv='mv -i'
alias rm='rm -i'
alias config='/run/current-system/sw/bin/git --git-dir=$HOME/.myconf/ --work-tree=$HOME'
alias dotgit='/run/current-system/sw/bin/git --git-dir=$HOME/.cfg/ --work-tree=/'
alias msfconsole="msfconsole --quiet -x \"db_connect shovanrai@msf\""
alias graph='git log --decorate --all --graph'
#alias ls='ls --color=auto'
alias ls='exa'
alias hgr='history | grep'
alias g='goto'

# SOURCE
source /home/shovanrai/scripts/goto.sh
#source /home/shovanrai/source_file/bash-wakatime/bash-wakatime.sh
#. /home/shovanrai/.nix-profile/etc/profile.d/nix.sh # added by Nix installer

# CARDANO
# cardano-node 1.29.0 alonz0-testnet
# export CARDANO_NODE_SOCKET_PATH=/home/shovanrai/cardano/db/node.socket
# cardano-node 1.29.0 alonz0-mainnet
export CARDANO_NODE_SOCKET_PATH=/home/shovanrai/cardano/mainnet/db/node.socket

# FUNCTIONS
# se() { du -a ~/ --exclude=arch-root | awk '{print $2}' | fzf | xargs -o $EDITOR ;}
se() { nvim $(rg ~/ --files --hidden -g '!arch-root' -g '!Storage'| fzf --height 40% --layout=reverse --preview '(highlight -O ansi {} || cat {}) 2> /dev/null | head -500') ;}
ccd() { cd $(rg ~/ --hidden --files --null -g '!arch-root'| xargs -0 dirname | uniq | fzf --height 100%) ;}
# vf() { fzf | xargs -o $EDITOR ;}

# FZF
if command -v fzf-share >/dev/null; then
  source "$(fzf-share)/key-bindings.bash"
  source "$(fzf-share)/completion.bash"
fi
# export FZF_DEFAULT_OPTS="--height 40% --layout=reverse --preview '(highlight -O ansi {} || cat {}) 2> /dev/null | head -500'"
# export FZF_DEFAULT_COMMAND="rg ~/ --files --hidden -g '!arch-root'"
# export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
# export FZF_ALT_C_COMMAND="$FZF_DEFAULT_COMMAND"
# alias s='rg --files --hidden --follow --no-ignore-vcs -g "!{node_modules,.git}" | fzf'
# _fzf_compgen_path() {
#   rg ~/ --files --hidden --follow -g '!arch-root'
# }
# _fzf_compgen_dir() {
#     rg --hidden --files -g '!arch-root' --null | xargs -0 dirname | uniq
#   }

# POWERLINE
# if [ -f /run/current-system/sw/lib/python3.9/site-packages/powerline/bindings/bash/powerline.sh ]; then
#     /run/current-system/sw/bin/powerline-daemon -q
#     POWERLINE_BASH_CONTINUATION=1
#     POWERLINE_BASH_SELECT=1
#     source /run/current-system/sw/lib/python3.9/site-packages/powerline/bindings/bash/powerline.sh
# fi

# STARSHIP
eval "$(starship init bash)"
